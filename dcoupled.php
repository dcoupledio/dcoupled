<?php

use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;

class dcoupled{

	protected static $app;

	public static function app()
	{
		if( !isset(self::$app) )
		{
			self::$app = new Application( new ApplicationContainer() );
		}

		return self::$app;
	}
}