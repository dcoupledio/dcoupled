<?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;

class DTest extends TestCase{

    public function testCanGetAppViaStaticMethod()
    {
        $app = dcoupled::app();

        $this->assertInstanceOf( Application::class, $app );
    }

}